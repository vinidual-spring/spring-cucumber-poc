FROM openjdk:13-jdk-oracle
VOLUME /tmp
ADD /target/cucumber-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]